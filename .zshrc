export ZSH="/home/teur/.oh-my-zsh"
ZSH_THEME="agnoster"
plugins=(git)
source $ZSH/oh-my-zsh.sh
source $HOME/scritps/clean.sh
export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8
source ~/.profile
alias ls="exa -l"
alias lsa="exa -la"
alias vim="nvim"
alias ts="tmux new -s"
alias e="exit"
alias c="clear"
alias t="tmux"
alias clean=run
