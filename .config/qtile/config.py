from typing import List  # noqa: F401

from libqtile import bar, layout, widget, extension
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()

colors = ["#2e3440", "#68809a", "#8c738c"]

keys = [
    Key(['mod4'], 'd', lazy.run_extension(extension.DmenuRun(
        dmenu_prompt="find: ",
        background=colors[0],
        foreground=colors[2],
        selected_background=colors[1],
        selected_foreground="#ffffff",
        fontsize=14
    ))),

    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    Key([mod, "shift"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
    Key([mod, "shift"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),

    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "p", lazy.spawn('scrot'), desc="make screnshoot"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown qtile"),
]


def init_group_names():
    return [("WWW", {'layout': 'max'}),
            ("DEV", {'layout': 'monadtall'}),
            ("SYS", {'layout': 'tile'}),
            ("DOC", {'layout': 'max'}),
            ("CHAT", {'layout': 'max'})]


def init_groups():
    return [Group(name, **kwargs) for name, kwargs in group_names]


if __name__ in ["config", "__main__"]:
    group_names = init_group_names()
    groups = init_groups()

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))

layouts = [
    layout.Max(),
    layout.MonadTall(
        border_focus=colors[2],
        border_width=3,
        margin=10),
    layout.MonadWide(
        border_focus=colors[2],
        border_width=3,
        margin=10),
    layout.RatioTile(
        border_focus=colors[2],
        border_width=3,
        margin=10),
    layout.Tile(
        border_focus=colors[2],
        border_width=3,
        margin=10),
]


widget_defaults = dict(
    font='mononoki Nerd Font Mono',
    fontsize=12,
    padding=3,
    background=colors[0],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(hide_unused=True,
                                highlight_color=colors[2],
                                highlight_method="line",
                                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#d8dee9", "#a5abb6"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox(
                    text="", foreground=colors[1], fontsize=23, margin=0, padding=0),
                widget.Volume(background=colors[1]),
                widget.TextBox(
                    text="", foreground=colors[2], background=colors[1], fontsize=23, margin=0, padding=0),
                widget.CurrentLayout(background=colors[2]),
                widget.TextBox(
                    text="", background=colors[2], foreground=colors[1], fontsize=23, margin=0, padding=0),
                widget.Battery(background=colors[1]),
                widget.TextBox(
                    text="", background=colors[1], foreground=colors[2], fontsize=23, margin=0, padding=0),
                widget.KeyboardLayout(
                    background=colors[2], configured_keyboards=['us', 'ru']),
                widget.TextBox(
                    text="", background=colors[2], foreground=colors[1], fontsize=23, margin=0, padding=0),
                widget.Clock(format='%c',
                             background=colors[1],),
                widget.TextBox(
                    text="", background=colors[1], foreground=colors[2], fontsize=23, margin=0, padding=0),
                widget.QuickExit(default_text="", fontsize=15,
                                 background=colors[2]),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"
wmname = "hackable wm"
