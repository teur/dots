source $HOME/.config/nvim/defaults.vim
source $HOME/.config/nvim/plugs.vim
source $HOME/.config/nvim/ui.vim
source $HOME/.config/nvim/maps.vim
source $HOME/.config/nvim/lsp.vim
