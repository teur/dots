" defaults
set noerrorbells
set nobackup 
set encoding=UTF-8
set conceallevel=1
set ttimeoutlen=50
set notermguicolors
set undodir=~/.vim/undodir
set undofile
set hidden
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set nocompatible
set syntax
set laststatus=2
set noshowmode
set showtabline=2
set mouse=a

" numbers
set number relativenumber

" tab
set smarttab
set expandtab
autocmd BufNewFile,BufRead *.py set tabstop=4
autocmd BufNewFile,BufRead *.py set softtabstop=4
autocmd BufNewFile,BufRead *.py set shiftwidth=4
set tabstop=2
set softtabstop=2
set shiftwidth=2
set smartindent

" search
set incsearch
set hlsearch
set smartcase
