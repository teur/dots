# Dots

My default configuration files for **unix-like** systems

* ## Qtile
    ![qtile](screens/2020-12-03-175053_1920x1080_scrot.png)
* ## Neovim
    ![vim](screens/2020-12-03-173504_1920x1080_scrot.png)
* ## Tmux
    ![tmux](screens/2020-12-03-173501_1920x1080_scrot.png)