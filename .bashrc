source ~/.profile
source /usr/share/doc/pkgfile/command-not-found.bash
source ~/.bash_profile

PS1="\[\e[93m\]<\[\e[92m\]\u\[\e[91m\]@\[\e[92m\]\h\[\e[93m\] \[\e[95m\]\\W\[\e[93m\]> \[\e[97m\]\$ "

set -o vi

alias ls="exa -l"
alias lsa="exa -la"
alias vim="nvim"
alias ts="tmux new -s"
alias e="exit"
alias c="clear"
alias t="tmux"
