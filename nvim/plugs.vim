call plug#begin('~/.vim/plugged')
  Plug 'morhetz/gruvbox'
  Plug 'sheerun/vim-polyglot'
  Plug 'mhinz/vim-startify'
  Plug 'itchyny/lightline.vim'
  Plug 'preservim/nerdtree'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'Yggdroot/indentLine'
  Plug 'ryanoasis/vim-devicons'
call plug#end()

