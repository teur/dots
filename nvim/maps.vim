" autoclosing brackets /tags
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap ` ``<left>

inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

noremap <C-n> :NERDTreeToggle<CR>
